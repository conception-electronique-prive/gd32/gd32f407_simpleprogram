/**
 * @file    main.cpp
 * @author  Samuel Martel
 * @date    2022/01/18
 * @brief   Example program to show how to use external interrupts using the GD32F407R-START
 * starter kit.
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/<a/>.
 */

#include "main.h"

#include "gd32f4xx.h"
#include "gd32f4xx_gpio.h"
#include "gd32f4xx_misc.h"
#include "gd32f4xx_rcu.h"
#include "gd32f4xx_syscfg.h"

#include "sys/systick.h"

static void InitClocks();
static void InitPeripherals();

extern "C" [[maybe_unused]] void EXTI0_IRQHandler(void);

int main()
{
    systick_config();

    InitClocks();
    InitPeripherals();

    volatile bool t = true;
    while (t)
    {
        FlagStatus s = gpio_input_bit_get(BUTTON_PORT, BUTTON_PIN);
        gpio_bit_write(LED_PORT, LED_PIN, s);
    }
}

void InitClocks()
{
    // LED is on port C.
    rcu_periph_clock_enable(RCU_GPIOC);

    // Button is on port A.
    rcu_periph_clock_enable(RCU_GPIOA);

    // SYSCONFIG handles the interrupts.
    rcu_periph_clock_enable(RCU_SYSCFG);
}

void InitPeripherals()
{
    // Resets the GPIO ports to their default states.
    gpio_deinit(LED_PORT);
    gpio_deinit(BUTTON_PORT);


    // Initialize the LED.
    gpio_mode_set(LED_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, LED_PIN);
    gpio_output_options_set(LED_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, LED_PIN);

    // We want the LED to be ON by default.
    gpio_bit_write(LED_PORT, LED_PIN, SET);

    // Initialize the button as an input.
    gpio_mode_set(BUTTON_PORT, GPIO_MODE_INPUT, GPIO_PUPD_NONE, BUTTON_PIN);

    // Enable the EXTI0_IRQn interrupt with high priority. EXTI0_IRQn is used because the button is
    // on PA0.
    nvic_irq_enable(EXTI0_IRQn, 0, 0);

    // Binds PA0 with EXTI0_IRQn.
    syscfg_exti_line_config(EXTI_SOURCE_GPIOA, EXTI_SOURCE_PIN0);

    // Configure EXTI0_IRQn to trigger an interruption on both fronts.
    exti_init(EXTI_0, EXTI_INTERRUPT, EXTI_TRIG_BOTH);

    // Clear the interrupt flag to get us started.
    exti_interrupt_flag_clear(EXTI_0);
}

extern "C" [[maybe_unused]] void EXTI0_IRQHandler(void)
{
    // Verify the source of the interrupt (only useful when more than one thing can trigger the same
    // interrupt).
    if (exti_interrupt_flag_get(EXTI_0))
    {
        // Get the state of the pin.
        FlagStatus s = gpio_input_bit_get(BUTTON_PORT, BUTTON_PIN);

        // Set the LED in the same state.
        gpio_bit_write(LED_PORT, LED_PIN, s);
    }
    // It's important to clear the flag!
    exti_interrupt_flag_clear(EXTI_0);
}
