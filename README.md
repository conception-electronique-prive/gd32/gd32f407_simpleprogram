# GD32F407_SimpleProgram

Simple applications for the GD32F407 in Clion.

## Cloning

```
git clone https://gitlab.com/conception-electronique-prive/gd32/gd32f407_simpleprogram --recursive
```

## In this repository
- [GPIO as inputs and outputs](https://gitlab.com/conception-electronique-prive/gd32/gd32f407_simpleprogram/-/tree/master/src/GPIO)
- [External interrupts](https://gitlab.com/conception-electronique-prive/gd32/gd32f407_simpleprogram/-/tree/master/src/EXTI)
